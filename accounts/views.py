# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import View, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import UserCreateForm, UserLoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.urls import reverse, reverse_lazy
from django.db import transaction
from forms import  ContactForm, UserForm, FindRoomForm, ReserveRoomForm
from django.contrib import messages
from models import Room, Reservation, Payment
from django.core.mail import send_mail, BadHeaderError
from django.db.models import Q


@login_required
def logout_view(request):
    logout(request)
    return redirect('accounts:index')


def reservation_detail_view(request, pk):
    reservation = Reservation.objects.get(id=pk)
    return render(request, 'accounts/reservation_detail.html', {'reservation': reservation})


@login_required
def my_view2(request):
    reservations = Reservation.objects.filter(client=request.user.pk)
    return render(request, 'accounts/profile.html', {'reservations': reservations})


class FindRoomView(View):
    form = FindRoomForm
    template_name = 'accounts/index.html'

    def get(self, request):
        if request.user.is_authenticated():
            form = self.form
            return render(request, self.template_name, {'form': form})
        else:
            return redirect('accounts:login')

    def post(self, request):
        form = FindRoomForm(request.POST)
        if form.is_valid():
            from_date = form.cleaned_data['start_date']
            to_date = form.cleaned_data['end_date']
            dates = [from_date, to_date]

            room_type = form.cleaned_data['type']
            rooms_with_no_reservation = Room.objects.filter(Q(reservation__pk__isnull=True))
            rooms_with_valid_free_date = Room.objects.exclude((  (Q(reservation__end_date__gte=from_date) & Q(reservation__end_date__lte=to_date))
                                          | (Q(reservation__start_date__gte=from_date) & Q(reservation__start_date__lte=to_date))
                                          | (Q(reservation__start_date__lte=from_date) & Q(reservation__end_date__gte=to_date))))
            query_result = (rooms_with_no_reservation | rooms_with_valid_free_date).filter(Q(type=room_type)).distinct()

            return render(request, 'accounts/room_list.html', {'rooms': query_result})
        else:
            return render(request, self.template_name, {'form': form})


class RoomReserveView(View):
    form = ReserveRoomForm
    template_name = 'accounts/room_reserve.html'
    room_id = Room.objects.all()
    success_template = 'accounts/room_reserve_success.html'

    def get(self, request, pk):
        form = self.form
        room = Room.objects.get(id=pk)
        return render(request, self.template_name, {'form': form,
                                                    'room': room})

    def post(self, request, pk):
        form = ReserveRoomForm(request.POST)
        if form.is_valid():
            from_date = form.cleaned_data['start_date']
            to_date = form.cleaned_data['end_date']
            payment_method = form.cleaned_data['payment_method']
            room_id = Room.objects.get(id=pk)
            rooms_with_valid_free_date = Room.objects.filter(
                ((Q(reservation__end_date__gte=from_date) & Q(reservation__end_date__lte=to_date))
                 | (Q(reservation__start_date__gte=from_date) & Q(reservation__start_date__lte=to_date))
                 | (Q(reservation__start_date__lte=from_date) & Q(reservation__end_date__gte=to_date)))).distinct()
            if rooms_with_valid_free_date.filter(pk=room_id.pk).exists():
                return render(request, self.template_name, {'form': form})
            else:
                q = Reservation.add_reservation(Reservation(), end_date=to_date, start_date=from_date,
                                                client=request.user, room=room_id)
                p = Payment.add_payment(Payment(), q, payment_method)
                return render(request, self.success_template, {'reservation': q,
                                                               'payment': p})
        else:
            return render(request, self.template_name, {'form': form})


class UserCreateFormView(View):
    user_form = UserCreateForm
    template_name = 'accounts/register_form.html'

    def get(self, request):
        if request.user.is_authenticated():
            return redirect('accounts:index')
        else:
            user_form = self.user_form
            return render(request, self.template_name, {'user_form': user_form})

    def post(self, request):
        user_form = UserCreateForm(request.POST)
        if user_form.is_valid():
            user = user_form.save()
            if user is not None:
                login(request, user)
                return redirect('accounts:index')
        else:
            return render(request, self.template_name, {'user_form': user_form})


class UserLoginFormView(View):
    form = UserLoginForm
    template_name = 'accounts/login_form.html'

    def get(self, request):
        if request.user.is_authenticated():
            return redirect('accounts:index')
        else:
            form = self.form
            return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                login(request, user)
                return redirect('accounts:index')
        else:
            return render(request, self.template_name, {'form': form})


def send_mail_view(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['mail@hotel.pod.pies'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('accounts:index')
    return render(request, "accounts/send_mail.html", {'form': form})


def gallery_view(request):
    return render(request, 'accounts/gallery.html')


@login_required
def room_view(request):
    rooms = Room.objects.all()
    return render(request, 'accounts/room_list.html', {'rooms': rooms})


class RoomDetailView(DetailView):
    model = Room
    template_name = 'accounts/room_detail.html'


class ReservationDetailView(DetailView):
    model = Room
    template_name = 'accounts/reservation_detail.html'


@login_required
def users_list_view(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            return redirect('/admin')
        else:
            return my_view2(request)


def contact_view(request):
    return render(request, 'accounts/contact.html')


