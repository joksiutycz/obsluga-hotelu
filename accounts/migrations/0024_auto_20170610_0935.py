# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-10 09:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0023_auto_20170606_1949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='type',
            field=models.CharField(choices=[('1', '1-osobowy'), ('2', '2-osobowy'), ('3', '3-osobowy'), ('4', '4-osobowy')], default=None, max_length=9, verbose_name='Rodzaj'),
        ),
    ]
