# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-05 21:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20170605_2032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reservation',
            name='test',
        ),
        migrations.AddField(
            model_name='room',
            name='type',
            field=models.CharField(choices=[('1O', '1-osobowy'), ('2O', '2-osobowy'), ('3O', '3-osobowy')], default=None, max_length=9),
        ),
    ]
