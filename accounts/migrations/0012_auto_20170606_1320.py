# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-06 13:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_profile_role'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='is_employee',
        ),
        migrations.AlterField(
            model_name='profile',
            name='role',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Student'), (2, 'Teacher')], null=True),
        ),
    ]
