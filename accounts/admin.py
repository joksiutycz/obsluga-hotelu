# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import User
from .models import Profile, Room, Vacation, Payment, Reservation, Salary


class PaymentInLine(admin.StackedInline):
    model = Payment
    can_delete = True
    verbose_name_plural = u'Platności'
    verbose_name = u'Płatność'
    fk_name = 'reservation'
    extra = 1


class ProfileInLine(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "Profil"
    fk_name = 'user'


class SalaryInLine(admin.StackedInline):
    model = Salary
    can_delete = False
    verbose_name = u'Wypłata'
    verbose_name_plural = u"Wypłaty"
    fk_name = 'employee'
    extra = 1


class ReservationInLine(admin.StackedInline):
    model = Reservation
    can_delete = True
    verbose_name = u'Rezerwacja'
    verbose_name_plural = u'Rezerwacje'
    fk_name = 'client'
    extra = 1


class VacationInLine(admin.StackedInline):
    model = Vacation
    can_delete = True
    verbose_name = u'Urlop'
    verbose_name_plural = u"Urlopy"
    fk_name = 'employee'
    extra = 1


class RoomAdmin(admin.ModelAdmin):
    fieldsets = [
        (u'Pokój', {'fields': ['type', 'price', 'is_occupied']}),
        #(u'Informacje o rezerwacjach', {'fields': ['price']} ),
    ]
    list_display = ('pk', 'type', 'price', 'is_occupied')


class PaymentAdmin(admin.ModelAdmin):
    fieldsets = [
        (u'Płatność', {'fields': ['reservation', 'price', 'discount', 'is_paid']})
    ]
    list_display = ('pk', 'reservation', 'price', 'discount', 'is_paid')


class ReservationAdmin(admin.ModelAdmin):
    inlines = (PaymentInLine, )
    fieldsets = [
        (u'Rezerwacja', {'fields': ['room', 'client', 'start_date', 'end_date', 'is_confirmed']})
    ]
    list_display = ('pk', 'client', 'room', 'start_date', 'end_date', 'is_confirmed')


class CustomUserAdmin(admin.ModelAdmin):
    inlines = (ProfileInLine, SalaryInLine, ReservationInLine, VacationInLine )
    list_display = ('username', 'email', 'first_name', 'last_name', 'get_role')
    list_select_related = ('profile', )

    def get_role(self, instance):
        return instance.profile.role

    get_role.short_description = 'Kto'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Vacation)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Reservation, ReservationAdmin)


