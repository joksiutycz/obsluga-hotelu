# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms
from django.contrib.auth.models import User
from models import Profile, Room, Reservation, TYPE_CHOICES, PAYMENT_CHOICES
from django.contrib.admin.widgets import AdminDateWidget
import datetime
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget


class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email', 'first_name', 'last_name']
        help_texts = {
            'username': None,
            'password1': None,
            'password2': None,
            'email': None,
            'first_name': None,
            'last_name': None
        }
        labels = {
            'username': u'Nazwa użytkownika',
            'password1': u'Hasło',
            'password2': u'Powtórz hasło',
            'email': u'Adres e-mail',
            'first_name': u'Imię',
            'last_name': u'Nazwisko'
        }


class UserLoginForm(AuthenticationForm):
    pass


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ['hire_date']


class ContactForm(forms.Form):
    from_email = forms.EmailField(required=True)
    from_email.label = "Od:"
    subject = forms.CharField(required=True)
    subject.label = "Temat"
    message = forms.CharField(widget=forms.Textarea, required=True)
    message.label = u"Wiadomość"


class FindRoomForm(forms.Form):
    start_date = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3))
    end_date = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3))
    type = forms.CharField(max_length=9, widget=forms.Select(choices=TYPE_CHOICES), )
    start_date.label = u"Od"
    end_date.label = u"Do"
    type.label = u"Rodzaj pokoju"


class AddRoomForm(forms.Form):
    type = forms.CharField(max_length=9, widget=forms.Select(choices=TYPE_CHOICES), )
    type.label = u"Rodzaj pokoju"
    price = forms.IntegerField(max_value=5000)
    price.label = u"Cena/noc"


class ReserveRoomForm(forms.Form):
    start_date = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3))
    end_date = forms.DateField(widget=DateWidget(usel10n=True, bootstrap_version=3))
    payment_method = forms.CharField(max_length=20, widget=forms.Select(choices=PAYMENT_CHOICES))
    payment_method.label = u"Sposób płatności"
    start_date.label = u"Od"
    end_date.label = u"Do"
















