# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete, pre_delete
from django.dispatch import receiver
from django.db.models import Q
import datetime


TYPE_CHOICES = (
    ('1', '1-osobowy'),
    ('2', '2-osobowy'),
    ('3', '3-osobowy'),
    ('4', '4-osobowy')
)

PAYMENT_CHOICES = (
    (1, u'Gotówka'),
    (2, u'Karta'),
    (3, u'Przelew'),
)


class Profile(models.Model):
    ROLE_CHOICES = (
        ('Klient', 'Klient'),
        ('Pracownik', 'Pracownik'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=(u'Użytkownik'))
    hire_date = models.DateField(null=True, blank=True, verbose_name=('Data zatrudnienia'))
    role = models.CharField(max_length=9, choices=ROLE_CHOICES, null=True, blank=True, verbose_name=(u"Kto"), default='Klient')

    @receiver(post_save, sender = User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return str(self.user.username)

    def is_employee(self):
        if self.role == 'Pracownik':
            return True
        else:
            return False

    class Meta:
        verbose_name = u'Profil'
        verbose_name_plural = u'Profile'


class Room(models.Model):
    type = models.CharField(max_length=9, choices=TYPE_CHOICES, default=None, verbose_name=(u'Rodzaj'))
    price = models.FloatField(default=None, verbose_name=('Cena/noc'))
    is_occupied = models.BooleanField(default=False, verbose_name=(u'Czy zajety'))

    class Meta:
        verbose_name = u'Pokój'
        verbose_name_plural = u'Pokoje'

    def __str__(self):
        return str(self.pk)

    def set_room_occupied(self):
        self.is_occupied = True

    def set_room_free(self):
        self.is_occupied = False


class Vacation(models.Model):
    employee = models.ForeignKey(User, on_delete=models.CASCADE, default=None, verbose_name=('Pracownik'))
    start_date = models.DateField(default=None, verbose_name=(u'Data rozpoczęcia'))
    end_date = models.DateField(default=None, verbose_name=('Data zakończenia'))

    class Meta:
        verbose_name = u'Urlop'
        verbose_name_plural = u'Urlopy'


class Reservation(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, default=None, verbose_name=u"Nr pokoju")
    client = models.ForeignKey(User, on_delete=models.CASCADE, default=None, verbose_name=u"Nazwa użytkownika")
    start_date = models.DateField(default=None, verbose_name=u"Data rozpoczęcia")
    end_date = models.DateField(default=None, verbose_name=u"Data zakończenia")
    is_confirmed = models.BooleanField(default=False, verbose_name=u'Czy potwierdzona')

    class Meta:
        verbose_name = u'Rezerwacja'
        verbose_name_plural = u'Rezerwacje'

    def __str__(self):
        return str(self.pk)

    def add_reservation(self, end_date, start_date, client, room):
        self.end_date = end_date
        self.start_date = start_date
        self.client = client
        self.room = room
        self.room.set_room_occupied()
        self.room.save()
        self.save()
        return self

    def get_reservations(self):
        return Reservation.objects.all()

    def confirm_reservation(self):
        self.is_confirmed = True


@receiver(post_delete, sender=Reservation)
def _reservation_delete(sender, instance, **kwargs):
    q = instance.room.pk
    if not Reservation.objects.filter(Q(room_id=q)).exists():
        r = Room.objects.get(id=q)
        r.set_room_free()
        r.save()


class Payment(models.Model):
    reservation = models.OneToOneField(Reservation, default=None, on_delete=models.CASCADE, verbose_name=(u'Nr rezerwacji'))
    price = models.FloatField(default=None, verbose_name=('Koszt'))
    discount = models.FloatField(default=0.0, verbose_name=(u'Zniżka'))
    is_paid = models.BooleanField(default=False, verbose_name=u"Czy opłacone")
    method = models.SmallIntegerField(default=0, choices=PAYMENT_CHOICES, verbose_name=u'Sposób płatności')

    class Meta:
        verbose_name = u'Płatność'
        verbose_name_plural = u'Płatności'

    def __str__(self):
        return str(self.pk)

    def add_payment(self, reservation, method):
        self.reservation = reservation
        self.price = self.calculate_price()
        self.method = method
        self.save()
        return self

    def calculate_price(self):
        days = (self.reservation.end_date - self.reservation.start_date).days
        price = float(days) * float(self.reservation.room.price)
        print(price)
        return price

    def confirm_payment(self):
        self.is_paid = True
        self.save()


class Salary(models.Model):
    employee = models.ForeignKey(User, on_delete=models.CASCADE, default=None, verbose_name=('Pracownik'))
    size = models.FloatField(default=1200.0, verbose_name=u"Ile")
    bonus = models.FloatField(default=0, verbose_name=u'Premia')
    month = models.DateField(default=None, verbose_name=u'Miesiąc')

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = u'Wypłata'
        verbose_name_plural = u'Wypłaty'

    def calculate_bonus(self):
        pass




