from django.conf.urls import url

from . import views
from django.contrib.auth.decorators import login_required

app_name = 'accounts'

urlpatterns = [
    url(r'^register/$', views.UserCreateFormView.as_view(), name='register'),
    url(r'^login/$', views.UserLoginFormView.as_view(), name='login'),
    url(r'^$', views.FindRoomView.as_view(), name='index'),
    url(r'^profile/$', views.users_list_view, name='profile'),
    url(r'^profile/reservation/(?P<pk>[0-9]+)$', views.reservation_detail_view, name='reservation-detail'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^contact/$', views.contact_view, name='contact'),
    url(r'^mail/$', views.send_mail_view, name='mail'),
    url(r'^gallery/$', views.gallery_view, name='gallery'),
    url(r'^room_list/$', views.room_view, name='room-list'),
    url(r'^room_detail/(?P<pk>[0-9]+)/$', login_required(views.RoomDetailView.as_view()), name='room-detail'),
    url(r'^room_detail/(?P<pk>[0-9]+)/reserve/$', login_required(views.RoomReserveView.as_view()), name='reserve-room'),
]